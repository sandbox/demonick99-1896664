AUDIOFIELD_ADDFORMAT.MODULE
--------------------------------------------------------------------------------
AudioField additional formatters module is an add-on for the AudioField module, which adds additional output formats fields type AudioField. Allows you to select the format of the output fields AudioField for each field separately, and not only for all the fields at once.

Developed by
--------------------------------------------------------------------------------
Nicholas Novoselov

Installation
--------------------------------------------------------------------------------
See the Documentation.

Dependencies
--------------------------------------------------------------------------------
AudioField additional formatters 6.x depends on the AudioField modules.

Capabilities of the module
--------------------------------------------------------------------------------
After installation of the module for the AudioField in the list select the display format of the field appears the new values for all the players.

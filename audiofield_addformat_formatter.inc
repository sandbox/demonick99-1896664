<?php

/**
 * Formatter theme function for file fields presented as a Flash MP3 Player and Download link.
 */
function theme_audiofield_addformat_formatter_audiofield_embedded($element) {
  global $user;
  //Don't render player if there is no file.
  if (!$element['#item']['filepath']) {
    return '';
  }

  $audiofile = file_create_url($element['#item']['filepath']);
  $output = audiofield_addformat_get_player_by_name($audiofile, $element['#formatter']);

  if (!empty($element['#item']['data']['description'])) {
    $output .= '<div class="audio-description">' . $element['#item']['data']['description'] . '</div>';
  }

  //If user is file owner check for download permission and display download link
  if ($user->uid == $element['#item']['uid'] && user_access('download own audio files')) {
    $output .= '<div class="audio-download"><b>' . t('Download') . ':</b>' . theme('filefield_file', $element['#item']) . '</div>';
  }
  elseif (user_access('download all audio files')) {
    //If user has permission to download all audio files, then display download link
    $output .= '<div class="audio-download"><b>' . t('Download') . ':</b>' . theme('filefield_file', $element['#item']) . '</div>';
  }

  return $output;
}

/**
 * Formatter theme function for file fields presented as a Flash MP3 Player.
 */
function theme_audiofield_addformat_formatter_audiofield($element) {
  global $user;
  //Don't render player if there is no file.
  if (!$element['#item']['filepath']) {
    return '';
  }

  $audiofile = file_create_url($element['#item']['filepath']);
  $output = audiofield_addformat_get_player_by_name($audiofile, $element['#formatter']);

  if (!empty($element['#item']['data']['description'])) {
    $output .= '<div class="audio-description">' . $element['#item']['data']['description'] . '</div>';
  }

  return $output;
}
